#ifndef __AKBARFUNCTIONS_H
#define __AKBARFUNCTIONS_H


//============================== < DEFINES OF FUNCTIONS.C > ===========================================================
#define SendToBuffer(b)										\
			for(BYTE i = 0 ; i < 8 ; i++)					\
			{												\
				for (BYTE j = 0; j < 6; j++)				\
				{											\
					buffer[i][j] = b[i][j];					\
				}											\
			}
#define SendToShiftBuffer(b,c)								\
			for(BYTE i = 0 ; i < NumRows ; i++)				\
			{												\
				for (BYTE j = 0; j < c; j++)				\
				{											\
					shiftBuffer[i][j] = b[i][j];			\
				}											\
			}
//============================== < VARIABLES TO FUNCTIONS.C > ===========================================================


//--------------------------------- < TIMERS > --------------------------------------------------------------------------
WORD msTMR_Del = 0;
DWORD msTMR1;
DWORD msTMR2;
DWORD msTMR3;
DWORD msTMR4;
DWORD msTMR5;
DWORD msTMR6;
DWORD msTMR7;
DWORD msTMR8;

DWORD CountTick = 0;
DWORD Count10ms = 0;
DWORD Count100ms = 0;

//--------------------------------- < Flags > ---------------------------------------------------------------------------
BYTE ENC_OK;
BYTE Tick10ms = 0;
BYTE Tick100ms = 0;

volatile Word Flags0;
volatile Word Flags1;
volatile Word Flags;
//--------------------------------- < EXTERNAL VARIABLES TO FUNCTIONS.C > -----------------------------------------------
extern RTC_HandleTypeDef hrtc;
extern TIM_HandleTypeDef htim4;
extern UART_HandleTypeDef huart1;	//for communication with PC
extern UART_HandleTypeDef huart2;	//for MODBUS RTU

//--------------------------------- < INTERNAL VARIABLES TO FUNCTIONS.C > -----------------------------------------------
RTC_TimeTypeDef currentTime;
RTC_DateTypeDef currentDate;


volatile BYTE aRxBuffer1[BufferUart1];
volatile BYTE aTxBuffer2[BufferUart2Tx];
volatile BYTE aRxBuffer2[BufferUart2Rx];
volatile BYTE sReceivedText[BufferUart1];

WORD SizeOfStrMess;
BYTE UNUSEDPIN;
BYTE lineorder;
BYTE buffer[8][6]; //array[8][7][8] bitlardan iborat, butun ekranni bildiradi. har bir elementi yani BYTEi bitta 595 registr 
BYTE ledbuffer[7];
BYTE shiftBuffer[8][BUFFERSIZE + 1];
BYTE tempShifted[8][6];



//============================== < FUNCTION PROTOTYPES OF FUNCTIONS.C > ==================================================
//---------------------------------- < HIGH LEVEL FUNCTIONS > ------------------------------------------------------------
static inline void UpdateShiftBuffer(void);
static inline void pseudo_thread4_UpdDisp(void);

//---------------------------------- < MID LEVEL FUNCTIONS > -------------------------------------------------------------
static inline void myPrintf(char*, WORD);
static inline void HorShift(DWORD, WORD);
static inline void updateTimers(void);

//---------------------------------- < LOW LEVEL FUNCTIONS > -------------------------------------------------------------
static inline void ShiftLed(BYTE line);

//======================================== < CONSTANT VECTORS > ==========================================================



#endif // !__AKBARFUNCTIONS_H
