/* ----------------------- Platform includes --------------------------------*/
#include "port.h"
#include "stm32f1xx_hal.h"
/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"

/* ----------------------- static functions ---------------------------------*/
static void prvvTIMERExpiredISR(void);
void HAL_TIM_PeriodElapsedCallbackTim3(TIM_HandleTypeDef *htim);

extern TIM_HandleTypeDef htim3;
volatile uint16_t timeout = 0;
volatile uint16_t counter = 0;

/* ----------------------- Start implementation -----------------------------*/
BOOL xMBPortTimersInit(USHORT usTim1Timerout50us)
{
	timeout = usTim1Timerout50us;
	return TRUE;
}

void vMBPortTimersEnable()
{
	/* Enable the timer with the timeout passed to xMBPortTimersInit( ) */
	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_5);
	counter = 0;
	HAL_TIM_Base_Start_IT(&htim3);
}

void vMBPortTimersDisable()
{
	/* Disable any pending timers. */
	HAL_TIM_Base_Stop_IT(&htim3);
}

/* Create an ISR which is called whenever the timer has expired. This function
 * must then call pxMBPortCBTimerExpired( ) to notify the protocol stack that
 * the timer has expired.
 */
static void prvvTIMERExpiredISR(void)
{
	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_5);
	(void)pxMBPortCBTimerExpired();
}

void HAL_TIM_PeriodElapsedCallbackTim3(TIM_HandleTypeDef *htim)
{
	if ((++counter) >= timeout)
	{
		prvvTIMERExpiredISR();
	}
}

