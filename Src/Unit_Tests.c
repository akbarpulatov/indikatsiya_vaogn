//Instrunction to the Unit Tests
//Uncomment the functions that want to test

#include "main.h"
#include "unittests.h"

void __unitT_HardwareTimer(void) // Tests the timer is calling back or not
{
	static WORD tempcounter;
	tempcounter++;
	if (tempcounter > 500)
	{
		tempcounter = 0;
		LEDG ^=1;
		printf("\nTimer is working properly!");
	}
}
