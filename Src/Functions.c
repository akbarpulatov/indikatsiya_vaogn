#include "main.h"
#include "Functions.h"
#include "Font.h"
#include "unittests.h"
#include "port.h"
#include "mbport.h"
#include "mb.h"

#define REG_INPUT_START 1000	// Adress boshlanishi,
#define REG_INPUT_NREGS 64		//virtualniy tasavvurdagi registrlar soni

#define REG_TIME_START		2000
#define REG_TIME_NREGS		6
#define REG_TEXT_START		3000
#define REG_TEXT_NREGS		64

extern void HAL_TIM_PeriodElapsedCallbackTim3(TIM_HandleTypeDef *htim);

UCHAR MBdatatemp[20];
UCHAR TimeDateRegister[REG_TIME_NREGS];
UCHAR TextRegister[REG_TEXT_NREGS];

static USHORT usRegInputStart = REG_INPUT_START;
static USHORT usRegInputBuf[REG_INPUT_NREGS];	

//============= < TOP LEVEL AND HARDWARE CALLBACK FUNCTIONS > ====================================================================
inline void myInit()		//Function which is called from main.c before loop for initializing
{
	DIR485 = 0;
	
//	HAL_UART_Receive_IT(&huart1, aRxBuffer1, BufferUart1);	//Waiting for UART in interrupt mode in the ISR routine
//	HAL_UART_Receive_IT(&huart2, aRxBuffer2, BufferUart2Rx); //Waiting for UART in interrupt mode in the ISR routine
	
	currentTime.Hours = 11;
	currentTime.Minutes = 10;
	currentTime.Seconds = 10;
	HAL_RTC_SetTime(&hrtc, &currentTime, RTC_FORMAT_BIN);
	
	currentDate.Year = 19;
	currentDate.Month = 11;
	currentDate.Date = 12;
	HAL_RTC_SetDate(&hrtc, &currentDate, RTC_FORMAT_BIN);
	
	eMBErrorCode    eStatus;

	eStatus = eMBInit(MB_RTU, 0x11, 0, 115200, MB_PAR_NONE);

	/* Enable the Modbus Protocol Stack. */
	eStatus = eMBEnable();

}

inline void myMainLoop() 	//Loop which is called from Main loop of main.c
{
	UpdateShiftBuffer();
	HorShift(50, SizeOfStrMess);
	
	
	if (TempTimer2 == 0)
	{
		
		TempTimer2 = 3000;
	}
	
	for (USHORT i = 0; i < REG_TEXT_NREGS; i++)
	{
		usRegInputBuf[i] = TextRegister[i];
	}
	
	(void)eMBPoll();
}

void HAL_IncTick(void)
{
	//__unitT_HardwareTimer();
	updateTimers();
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)		// Interrupts from Timers 
{
	if (htim->Instance == TIM4)		//Interrupt from Timer_4
		{
			//__unitT_HardwareTimer();
			pseudo_thread4_UpdDisp();
		}
		else if(htim->Instance == TIM3)	//Interrupt from Timer_3 
		{
			//__unitT_HardwareTimer();	
			HAL_TIM_PeriodElapsedCallbackTim3(htim);
		}
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)	//External Interrupt Function
{
	//	if (GPIO_Pin == )
	//	{
	//	}
	//	else if (GPIO_Pin == )
	//	{
	//	}
}

//void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) // UART received data
//{
//	
//	if (huart->Instance == USART1)
//	{
//		
//		for (BYTE i = 0; i < BufferUart1; i++)
//		{
//			
//			//Textni bufferdan o`qib olamiz
//			if(aRxBuffer1[i] == UARTSTX)
//			{
//				i++;
//
//				BYTE j = 0;
//				while (aRxBuffer1[i] != UARTETX && i < BufferUart1)
//				{
//					sReceivedText[j] = aRxBuffer1[i];
//					j++;
//					i++;
//				}
//				sReceivedText[j] = NULL;
//				i++;
//			}
//			
//			//Time ni o`qib oladi
//			if(aRxBuffer1[i] == UARTSI)
//			{
//				i++;
//				
//				//Hours ni chiqarvolamiz bufferdan
//				currentTime.Hours = (((aRxBuffer1[i] - '0') * 10) + ((aRxBuffer1[i + 1] - '0') * 1));
//				i += 2;
//				
//				//Minut ni chiqarvolamiz bufferdan
//				currentTime.Minutes = (((aRxBuffer1[i] - '0') * 10) + ((aRxBuffer1[i + 1] - '0') * 1));
//				i += 2;
//
//				//Sekund ni chiqarvolamiz bufferdan
//				currentTime.Seconds = (((aRxBuffer1[i] - '0') * 10) + ((aRxBuffer1[i + 1] - '0') * 1));
//				i += 2;
//			}
//			
//			//Date ni o`qib oladi
//			if(aRxBuffer1[i] == UARTSI)
//			{
//				i++;
//				
//				//Date ni chiqarvolamiz bufferdan
//				currentDate.Date = (((aRxBuffer1[i] - '0') * 10) + ((aRxBuffer1[i + 1] - '0') * 1));
//				i += 2;
//				
//				//Month ni chiqarvolamiz bufferdan
//				currentDate.Month = (((aRxBuffer1[i] - '0') * 10) + ((aRxBuffer1[i + 1] - '0') * 1));
//				i += 2;
//
//				//Year ni chiqarvolamiz bufferdan
//				currentDate.Year = (((aRxBuffer1[i] - '0') * 10) + ((aRxBuffer1[i + 1] - '0') * 1));
//				i += 2;
//			}
//		}
//		
//		
//		HAL_UART_Transmit_IT(&huart1, aRxBuffer1, BufferUart1);
//		
//		HAL_RTC_SetTime(&hrtc, &currentTime, RTC_FORMAT_BIN);
//		HAL_RTC_SetDate(&hrtc, &currentDate, RTC_FORMAT_BIN);
//		
//		HAL_UART_Receive_IT(&huart1, aRxBuffer1, BufferUart1);
//	}
//	else if (huart->Instance == USART2)
//	{
//		//Modbus bo`yicha buffer to`lishi bilan shu joyga keladi, obrabotka shu yerda ro`y beradi.
//		if(aRxBuffer2[0] == MODBUSID)
//		{
//			
//			
//		}
//		
//		
//	
//		
//		HAL_UART_Transmit_IT(&huart1, aRxBuffer2, BufferUart2Rx);
//		
//		
//		HAL_UART_Receive_IT(&huart2, aRxBuffer2, BufferUart2Rx);   //Waiting for UART in interrupt mode in the ISR routine
//	}
//	
//}

/*
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart->Instance == USART2)
	{
		DIR485 = 0;
	}
}
*///TxCpltCallbackFunction

void myErrorHandler(BYTE ErrorID)
{
	
}

//====================================== < HIGH LEVEL FUNCTIONS > ================================================================
static inline void UpdateShiftBuffer(void)	// Gets current time and date from RTC, message to be printed, generates required string and pushes to the shift buffer
{
	HAL_RTC_GetTime(&hrtc, &currentTime, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &currentDate, RTC_FORMAT_BIN);
	
	//sprintf of Sentence Chars
	char SentenChars[BUFFERSIZE + 1];
	
	SizeOfStrMess = sprintf(SentenChars, "      %s %d:%d:%d %d/%d/%s%d", sReceivedText, currentTime.Hours, currentTime.Minutes, currentTime.Seconds, currentDate.Date, currentDate.Month, "20", currentDate.Year);
	
	myPrintf(SentenChars, SizeOfStrMess);
}

void printfTablo(void)
{
	//("Toshkent-Bukhara   %t", time)
}

//-------------------------------------- < Tablo States > -----------------------------------------------------------------------


//====================================== < MID LEVEL FUNCTIONS > =================================================================
static inline void myPrintf(char *str, WORD size)	//gets the string(as pointer) with quantity of characters, and pushes to Shift Buffer
{
	BYTE SentenBuilt[NumRows][BUFFERSIZE];
	for (WORD n = 0; n < size; n++)
	{
		//SentenBuiltga qo`shilib boradi BYTE ma BYTE
		for(BYTE i = 0 ; i < NumRows ; i++)
		{
			SentenBuilt[i][n] = Fonts_LMS[(str[n])][i];
		}
	}
	SendToShiftBuffer(SentenBuilt, size);
}

static inline void HorShift(DWORD Delay, WORD shiftColNumber) //gets the segment of shift buffer and pushes to the buffer. Each time segment is cut by shift on 1 bit.
{
	if (Timer_Shift == 0)
	{
		static WORD shiftOrder;
		
		if (shiftOrder >= shiftColNumber * 8) shiftOrder = 0;
			
		for (BYTE i = 0; i < NumRows; i++)
		{
			for (BYTE j = 0; j < NumCols; j++)
			{
				tempShifted[i][j] = (shiftBuffer[i][(shiftOrder / 8) + j] << (shiftOrder % 8))  |  
									(shiftBuffer[i][(shiftOrder / 8) + j + 1] >> (8 - (shiftOrder % 8)));
			}
		}
		shiftOrder++;
		//printf("\n%d", shiftOrder);
		
		Timer_Shift = Delay;
	}
	SendToBuffer(tempShifted);
}

static inline void pseudo_thread4_UpdDisp(void)
{
	ShiftLed((lineorder++) % 8);
}

static inline void updateTimers(void) 	//Updates the timers
{
	CountTick++;
	if (CountTick >= 500)
	{
		CountTick = 0;
		Tick ^= 1;
		LEDG = Tick;
	}
	
	Count100ms++;
	if (Count100ms >= 100)
	{
		Count100ms = 0;
		Tick100ms ^= 1;
	}
	
	Count10ms++;
	if (Count10ms >= 10)
	{
		Tick10ms ^= 1;
		Count10ms = 0;
	}
	
	//------------------ Timers -------------------------------------------------------------------------------------------
	if(msTMR_Del > 0)msTMR_Del--;
	if (msTMR1 > 0)msTMR1--;
	if (msTMR2 > 0)msTMR2--;
	if (msTMR3 > 0)msTMR3--;
	if (msTMR4 > 0)msTMR4--;
	if (msTMR5 > 0)msTMR5--;
	if (msTMR6 > 0)msTMR6--;
	if (msTMR7 > 0)msTMR7--;
	if (msTMR8 > 0)msTMR8--;
}

//====================================== < LOW LEVEL FUNCTIONS > =================================================================	
static inline void ShiftLed(BYTE line)
{
	//Setting up the Sending Buffer according to the Buffer of the Display
	ledbuffer[6] = (1 << line);                                                     // Qator tanlandi, binaryda 1 turgan o`rin qator nomeri
	for(BYTE i = 0 ; i < 6 ; i++)
	{
		ledbuffer[i] = buffer[line][i];
	}
	
	//Send the Buffer to the external shift registers
	BYTE temp;
	for (BYTE i = 0; i < 7; i++)	//Oxirgi shift registrdan boshlab boradi jo`natishni 
		{
			temp = ledbuffer[i];
			for (BYTE j = 0; j < 8; j++)	//Oxirgidan boshlab keladi jo`natishni lekin oxirgi element LSB 0 o`rinda turgan elementdir
				{
					Ser_DS = !((temp & 0x80) >> 7);
					temp = (temp << 1);
					DelayNop10();
					DelayNop10();
					Ser_SH = 1;
					DelayNop10();
					DelayNop10();
					DelayNop10();
					Ser_SH = 0;
				}
		}
	Ser_ST = 1;
	DelayNop10();
	DelayNop10();
	DelayNop10();
	Ser_ST = 0;
}

void DelayNop(void)
{
	Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop();
	Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop();
	Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop();
	Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop();
	Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop();
	Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop();
	Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop();
	Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop();
}





















